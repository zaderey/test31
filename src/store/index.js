import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		cars: null
	},
	actions: {
		getCars({ commit }) {
			axios.get('http://am111.05.testing.place/api/v1/cars/list?page=1&items=10')
			.then((response) => {
				commit('set_cars', response.data)
			})
		}
	},
	mutations: {
		set_cars(state, cars) {
			state.cars = cars;
		}
	}
});
