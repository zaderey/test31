import Vue from 'vue'
import App from './App.vue'
import Main from './views/main.vue'
import Car from './views/car.vue'
import VueRouter from 'vue-router'
import store from './store'

import 'bootstrap/dist/css/bootstrap.min.css'

Vue.config.productionTip = false

Vue.use(VueRouter)

const routes = [
		{ path: '/', component: Main },
		{ path: '/car/:id', component: Car },
]

const router = new VueRouter({
	routes
})

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
